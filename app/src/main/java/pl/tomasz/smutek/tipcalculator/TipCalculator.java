package pl.tomasz.smutek.tipcalculator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import pl.tomasz.smutek.tipcalculator.databinding.ActivityTipCalculatorBinding;

public class TipCalculator extends AppCompatActivity {

    ActivityTipCalculatorBinding layout;

    private static final int TIP_20 = 1;
    private static final double VALUE_FOR_TIP_20 = 0.2;
    private static final int TIP_18 = 2;
    private static final double VALUE_FOR_TIP_18 = 0.18;
    private static final int TIP_15 = 3;
    private static final double VALUE_FOR_TIP_15 = 0.15;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        layout = ActivityTipCalculatorBinding.inflate(getLayoutInflater());

        setContentView(layout.getRoot());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        layout.include.calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateForm()) {
                    Double amount = calculateAmountToPay();
                    showAmountToPay(amount);
                }
            }
        });


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.setVisibility(View.GONE);
    }

    private void showAmountToPay(Double amount) {
        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        if (layout.include.roundTipSwitch.isChecked()){
            decimalFormat = new DecimalFormat("0");
            decimalFormat.setRoundingMode(RoundingMode.UP);
        }

        String result = decimalFormat.format(amount);

        Toast.makeText(this, result + " $", Toast.LENGTH_LONG).show();
    }

    private boolean validateForm() {
        boolean result = true;
        int msgResource = 0;

        String amountStr = layout.include.amountText.getText().toString();

        if (amountStr == null || amountStr == ""){
            result = false;
            msgResource = R.string.validation_error_wrong_amount;
        }

        try {
            Double.parseDouble(amountStr);
        } catch (NumberFormatException e){
            result = false;
            msgResource = R.string.validation_error_wrong_amount;
        }

        if (layout.include.radioGroup.getCheckedRadioButtonId() == -1){
            result = false;
            msgResource = R.string.validation_error_no_tip;
        }

        if (result == false){
            Toast.makeText(this, msgResource, Toast.LENGTH_LONG).show();
        }

        return result;
    }

    private Double calculateAmountToPay() {
        String amountStr = layout.include.amountText.getText().toString();
        Double amount = Double.parseDouble(amountStr);

        Double tipPercent = 0.0;

        int radioButtonID = layout.include.radioGroup.getCheckedRadioButtonId();
        View radioButton = layout.include.radioGroup.findViewById(radioButtonID);
        int index = layout.include.radioGroup.indexOfChild(radioButton);

        switch (index){
            case TIP_20:
                tipPercent = VALUE_FOR_TIP_20;
                break;

            case TIP_18:
                tipPercent = VALUE_FOR_TIP_18;
                break;

            case TIP_15:
                tipPercent = VALUE_FOR_TIP_15;
                break;
        }

        amount = amount + amount * tipPercent;

        return amount;
    }
}